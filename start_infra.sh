#!/usr/bin/env sh
docker run --name redis -d -P redis 
docker run --name web1 -d --link redis:redis -P boujebba/poll 
docker run --name web2 -d --link redis:redis -P boujebba/poll
docker run -it -p 80:80 --link web1:web1 --link web2:web2 fedora22-proxy
