#!/usr/bin/env sh

docker stop web1 web2 redis
docker rm web1 web2 redis
