#!/usr/bin/env ruby

require 'sinatra'
require 'redis'

set :bind, '0.0.0.0'
$redis = Redis.new(:host => "redis")

get '/' do
  slim :index 
end

get '/:key' do
  increment params[:key]
  slim :score 
end

def increment key
  $redis.incr key
end

