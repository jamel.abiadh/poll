FROM boujebba/fedora22-ruby-to-build 
MAINTAINER Jamel Abiadh <jamel.abiadh@gmail.com>

EXPOSE 4567
USER ruby
WORKDIR /opt/ruby
CMD '/opt/ruby/poll.rb'
